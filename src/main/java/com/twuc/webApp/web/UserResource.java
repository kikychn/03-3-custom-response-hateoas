package com.twuc.webApp.web;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;

public class UserResource extends Resource<User> {

    public UserResource(User user, Link... links) {
        super(user, links);
    }
}
